import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import Ionic from '@ionic/vue';
import '@ionic/core/css/ionic.bundle.css';

import { library } from '@fortawesome/fontawesome-svg-core'
import { 
  faBuilding, faHome, faTachometerAlt, faBolt, faInfoCircle, 
  faCog, faMicrochip, faToggleOn, faBookmark, faStopwatch, 
  faChevronLeft, faLayerGroup, faStar, faSolarPanel, faExchangeAlt,
  faEllipsisH, faFileAlt, faPlus, faSnowplow } from '@fortawesome/free-solid-svg-icons'
import { 
  faBookmark as faBookmarkRegular, faStar as faStarRegular } from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import i18n from './i18n'


library.add(faBuilding, faHome,
  faTachometerAlt, faBolt, faInfoCircle, faCog, 
  faMicrochip, faToggleOn, faBookmark, faBookmarkRegular, faStopwatch, 
  faChevronLeft, faLayerGroup, faStar, faStarRegular, faSolarPanel, faExchangeAlt,
  faEllipsisH, faFileAlt, faPlus, faSnowplow)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.use(Ionic);

Vue.config.productionTip = false

const onDeviceReady = () => {
  new Vue({
    router,
    store,
    i18n,
    render: h => h(App)
  }).$mount('#app')
}

// Wait for the deviceready event to start the render
document.addEventListener("deviceready", onDeviceReady, false)

// If we are not in Cordova, manually trigger the deviceready event
const isCordovaApp = (typeof window.cordova !== "undefined");
if (!isCordovaApp){
  document.dispatchEvent(new CustomEvent("deviceready", {}));
}